if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()

" iA writer
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'reedes/vim-colors-pencil'
Plug 'subnut/vim-iawriter'

" color themes
Plug 'ayu-theme/ayu-vim'
Plug 'NLKNguyen/papercolor-theme'
Plug 'flrnd/candid.vim'

" language server
Plug 'neoclide/coc.nvim', {'branch': 'release'}
runtime coc-settings.vim

" file browsing
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'

" standard utilites
Plug 'tpope/vim-commentary'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'mileszs/ack.vim'

" visualize indentations
Plug 'Yggdroot/indentLine'
" do not conceal stuff in tex
let g:tex_conceal = 0

" parenthesis legibility
Plug 'frazrepo/vim-rainbow'
let g:rainbow_active = 1

" Syntax highlighting
Plug 'sheerun/vim-polyglot'

" note taking
Plug 'vimwiki/vimwiki'
Plug 'michal-h21/vim-zettel'

call plug#end()

set t_Co=256                   " 256 color terminal
set tabstop=8                  " tabs count as 8 columns
set softtabstop=2              " make tab in insert mode insert 2 spaces
set shiftwidth=2               " indent by 2 spaces when auto-indenting
set tw=80                      " text width to 72 for wrapping
set smartindent                " better autoindenting for code editing (especially for C)
set expandtab                  " replace actual tab character with spaces
set showmatch                  " show matching brackets
set incsearch                  " incremental searching
set hlsearch                   " highlight search matches
set ttimeoutlen=10             " escape modes immediately
set backspace=indent,eol,start " make backspace behave
set mouse=a                    " enable mouse
set ttymouse=xterm2            " hopefully allow vim term to scroll
set laststatus=2               " show status
set number                     " set line numbers on
set rulerformat=%55(%{strftime('%a\ %b\ %e\ %H:%M')}\ %5l,%-6(%c%V%)\ %P%) " display time in bottom line
set ruler                      " set (line,colum) in bottom right
set showcmd                    " show cmds in bottom right
syntax on                      " syntax highlighting
filetype plugin indent on      " activates indenting for files
set cursorline

set termguicolors
" let ayucolor="mirage"
" let ayucolor="dark"
" colorscheme ayu


" set background=dark
" colorscheme candid

set background=light
colorscheme PaperColor

" set vimwiki path and syntax
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md', 'auto_tags': 1}]

" ack -> ag
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" vim-commentary rules
autocmd FileType fsharp setlocal commentstring=//\ %s

" dont use arrow keys
" noremap <Up> <NOP>
" noremap <Down> <NOP>
" noremap <Left> <NOP>
" noremap <Right> <NOP>

" quick resize of splits
nnoremap - <C-W>10-
nnoremap _ <C-W>5-
nnoremap + <C-W>10+
nnoremap ? <C-W>5+
nnoremap < <C-W>10<
nnoremap > <C-W>10>

" how much a minimized split fills
set wmw=0
set wmh=0

" leader is space
let mapleader = "\<Space>"

" fzf settings
" nmap <C-o> :FZF %:p:h<CR>
nmap <C-p> :FZF<CR>
nmap <C-b> :Buffers<CR>

" ALT-x
nmap … :NERDTreeToggle<CR>

" ALT-g
nmap © :terminal<CR>

function Findo()
  let c_word = expand("<cword>")
  exe 'Ag ' . c_word
endfunction

" shortcuts
" ALT+f
nmap ƒ :Ag<CR>
" ALT+SHIFT+f
nmap ﬁ :call Findo()<CR>
" ALT+h
nmap « :lprev<CR>
" ALT+j
nmap ‹ :lopen<CR>
" ALT+k
nmap ∆ :VimwikiSearchTags 
" ALT+l
nmap ¬ :lnext<CR>

" configure syntax for files
" autocmd BufNewFile,BufRead *.pl set syntax=prolog
autocmd BufNewFile,BufRead *.fsx set ft=fsharp
autocmd BufNewFile,BufRead *.fs set ft=fsharp
autocmd BufNewFile,BufRead *.fsl set ft=Lex
autocmd BufNewFile,BufRead *.fsy set ft=Yacc
autocmd BufNewFile,BufRead *.tex set ft=tex

" ALT+v
nmap √ :!(until latexmk master.tex) && open master.pdf<CR><CR>
" nmap √ :!latexmk master.tex && open master.pdf<CR><CR>

" ALT+b, display for wiki backlinks 
nmap ∫ :VWB<CR>

" vim-fugitive binds
nmap gs :Git<CR>
nmap gp :Git push<CR>
nmap gl :Git pull<CR>
" ALT+n
nmap ñ :cprev<CR>
" ALT+m
nmap µ :cnext<CR>

" ALT+d 
nmap ∂ :!pandoc --from=markdown --to=pdf % > %.pdf && open %.pdf<CR><CR>

" ALT+q
nmap ° :Iawriter<CR>

" open vsp on right side, sp below
set splitbelow
set splitright
"
" change shape of cursor on insert mode
if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_SR = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=2\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_SR = "\<Esc>]50;CursorShape=2\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

set guifont=Monaco:h13
